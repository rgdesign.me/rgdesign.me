import { GlobalStyles } from 'twin.macro';
import '../styles/globals.css';
import TagManager from 'react-gtm-module';

const tagManagerArgs = {
  gtmId: 'GTM-PMWB2QC'
};

process.browser && TagManager.initialize(tagManagerArgs);

const App = ({ Component, pageProps }) => (
  <>
    <GlobalStyles />
    <Component {...pageProps} />
  </>
);

export default App;
