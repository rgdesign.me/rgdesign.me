import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
  host: process.env.EMAIL_HOST,
  port: process.env.EMAIL_PORT,
  secure: true,
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS
  }
});

export default async (req, res) => {
  const { buster } = req.body;

  const is_human_filled = await validator({ buster });
  if (!is_human_filled) {
    res.status(403).send('');
    return;
  } else {
    delete req.body.buster;
  }

  const is_valid = validator(req.body);
  if (!is_valid) {
    res.status(403).send('');
    return;
  }

  const { name, email, service } = req.body;

  const mailerRes = await mailer({
    to_name: name,
    to_email: email,
    service: service
  });

  res.send(mailerRes);
};

const validator = async (data) =>
  !Object.values(data).every((val) => val !== null && val !== '');

const mailer = ({ to_name, to_email, service }) => {
  const message = {
    replyTo: process.env.EMAIL_USER,
    from: process.env.EMAIL_USER, // sender address
    to: [
      {
        name: process.env.EMAIL_NAME,
        address: process.env.EMAIL_USER
      },
      {
        name: to_name,
        address: to_email
      }
    ], // list of receivers
    subject: `Hey ${
      to_name.split(' ')[0]
    } Let's talk about ${service.toLocaleLowerCase()}`,
    text: `I'll be contacting you in the following days business days for a free consultation.`,
    html: `
      <b>rgdesign.me</b>
      <br/>
      <p>Hey ${to_name.split(' ')[0]}!</p>
      <p>I'll be contacting you in the following days business days to to talk about ${service.toLocaleLowerCase()} and to schedule a free consultation.</p>
      <br/>
    `
  };

  return new Promise((resolve, reject) => {
    transporter.sendMail(message, (error, info) =>
      error ? reject(error) : resolve(info)
    );
  });
};
