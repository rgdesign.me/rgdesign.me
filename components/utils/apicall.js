const apicall = async ({
  data,
  endpoint,
  method = 'GET',
  url = 'https://www.rgdesign.me/api'
}) =>
  fetch(`${url}${endpoint}`, {
    method,
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body: JSON.stringify(data)
  })
    .then((res) => res.json())
    .catch(console.log);

export default apicall;
