import {
  Children,
  useEffect,
  useState,
  isValidElement,
  cloneElement
} from 'react';
import tw, { css } from 'twin.macro';
import useDebounce from '../hooks/useDebounce';

export const Form = ({
  onSubmit = () => {},
  className = '',
  children,
  id = `form-0`
}) => {
  const formInitialData = {};
  Children.forEach(
    children,
    (child) => (formInitialData[child.props.name] = child.props.defaultValue)
  );

  const [formData, setFormData] = useState(formInitialData);

  const handleChange = (name, value) => {
    setFormData({
      ...formData,
      [name]: value
    });
  };

  return (
    <form onSubmit={onSubmit} className={className} id={id}>
      {Children.map(children, (child) => {
        if (isValidElement(child)) {
          return cloneElement(child, {
            form: id,
            key: `${id}-${child.props.id}`,
            onChange: handleChange
          });
        }
      })}
    </form>
  );
};

export const Input = ({
  id,
  type,
  form = '',
  formState,
  buster = false,
  onChange = () => {},
  label = 'Label',
  placeholder = 'Placeholder...',
  required = false
}) => {
  const [edited, setEdited] = useState(false);
  const [error, setError] = useState('');
  const [defaultValue] = useState(formState[id]);
  const debounce = useDebounce(formState[id], 500);

  const handleChange = (e) => {
    e.preventDefault();
    console.log(id);
    const { name, value } = e.target;
    onChange(name, value);
  };

  useEffect(() => {
    if (formState[id] !== defaultValue && !edited) setEdited(true);
  }, [formState[id]]);

  useEffect(() => {
    if (edited) {
      handleValidation();
      console.log(formState[id], formState[id].length);
    }
  }, [debounce]);

  const handleValidation = () => {
    if (required && formState[id] === '') {
      setError('Required field');
    } else if (formState[id].length < 3 && formState[id] !== '') {
      setError('Length is too short!');
    } else {
      setError('');
    }
    formState[id] != defaultValue && !edited && setEdited(true);
  };

  return buster ? (
    <input
      className='no-access'
      id='buster'
      name='buster'
      form={form}
      type='text'
      defaultValue=''
      onChange={handleChange}
    />
  ) : (
    <div
      className='input'
      css={css`
        ${tw`
          flex
          flex-col
          m-2
          relative
          flex-auto
        `}
      `}
    >
      <label
        htmlFor={id}
        form={form}
        className='no-access'
        css={css`
          ${tw`
            justify-self-center
            self-center
            w-0
          `}
        `}
      >
        {label || id}
      </label>
      <span
        className='placeholder'
        css={css`
          ${tw`
            absolute
            duration-75
            pointer-events-none
            px-4
            py-2
            text-gray-lighter
            transition-opacity
          `}
          ${placeholder && formState[id] === ''
            ? tw`opacity-60`
            : tw`opacity-0`}
        `}
      >
        {placeholder}
      </span>
      {required && (
        <span
          css={css`
            ${tw`
              absolute
              h-1
              leading-none
              pointer-events-none
              right-2
              text-gray-lightest
              top-2
              w-1
            `}
          `}
        >
          *
        </span>
      )}
      <input
        css={css`
          ${tw`
            border
            border-gray-lightest
            flex-auto
            px-4
            py-2
            rounded-lg
            text-text
            transition
            
            hover:border-gray-lighter

            focus:border-primary
            focus:text-text
          `}
        `}
        id={id}
        name={id}
        form={form}
        type={type}
        defaultValue={defaultValue}
        onChange={handleChange}
        required={required}
      />
      <span
        className='error'
        aria-label={error}
        css={css`
          ${tw`
            -bottom-4
            absolute
            duration-75
            leading-none
            left-0
            px-4
            text-red-500
            text-xs
            transition-opacity
          `}
          ${error !== '' && edited ? 'opacity-100' : 'opacity-0'}
        `}
      >
        {error}
      </span>
    </div>
  );
};

const Select = ({ options = [] }) => {
  return <select></select>;
};
