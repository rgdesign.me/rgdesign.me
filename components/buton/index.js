import tw, { css } from 'twin.macro';

const Button = ({
  cta = false,
  style = 'flat',
  color = 'primary',
  label = 'button',
  onClick = () => {},
  children
}) => {
  return (
    <button
      aria-label={label}
      className={`${style} ${color} ${cta ? 'cta' : ''}`}
      css={css`
        ${tw`
          px-4
          py-2
          bg-primary
          text-white
          font-bold
          rounded-lg
          w-max
          m-2
          transition
          shadow-none

          hover:shadow-lg
          hover:bg-primary-dark
          
          active:bg-primary-darker
          active:shadow-none
        `}
      `}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
