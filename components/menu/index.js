import { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import { useScroll } from '../hooks/useScroll';
import Link from '../link';

const Menu = () => {
  const router = useRouter();
  const header = useRef();
  const { y } = useScroll({ wait: 0 });
  const [sticky, setSticky] = useState(false);
  const isSticky = () => y > header.current.offsetHeight;

  useEffect(() => {
    const stickyState = isSticky();
    stickyState != sticky && setSticky(stickyState);
  }, [y, header.current]);

  return (
    <header ref={header}>
      <div>
        <img />

        <div>
          <Link href='/'>
            <span>
              {y || 0}
            </span>
          </Link>
        </div>
      </div>
    </header>
  );
};

export default Menu;
