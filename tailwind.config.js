const colors = require('tailwindcss/colors');

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: (theme) => ({
        hero: "url('/herobg.jpg')"
      }),
      colors: {
        primary: {
          light: '#63a3fd',
          DEFAULT: '#3185FC',
          dark: '#046bfb',
          darker: '#1759b5'
        },
        secondary: {
          DEFAULT: '#2E2E2E'
        },
        text: '#2E2E2E',
        title: '#808080',
        gray: {
          DEFAULT: '#2E2E2E',
          lighter: '#808080',
          lightest: '#CCCCCC'
        }
      },
      width: {
        fit: 'fit-content'
      }
    }
  },
  variants: {
    extend: {
      borderColor: ['active'],
      padding: ['first', 'last', 'even', 'odd'],
      margin: ['first', 'last', 'even', 'odd']
    }
  },
  plugins: []
};
